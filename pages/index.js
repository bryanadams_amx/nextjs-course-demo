import { Fragment } from "react";
import Head from "next/head";
import { MongoClient } from "mongodb";

import MeetupList from "../components/meetups/MeetupList"

function HomePage(props) {

  return (
    <Fragment>
      <Head>
        <title>React Meetups</title>
        <meta 
          name='description'
          content='Coaba isi content meta'></meta>
      </Head>
      <MeetupList meetups={props.meetups} ></MeetupList>
    </Fragment>
  )
}

// export async function getServerSideProps(context) {
//   const req = context.req;
//   const res = context.res;

//   return{
//     props: {
//       meetups: dummy_meetups
//     }
//   };
// }

export async function getStaticProps() {

  const client = await MongoClient.connect(
    'mongodb+srv://bryanadams:bryanadams17amx@testcluster.xsofg.mongodb.net/meetups?retryWrites=true&w=majority');
  const db= client.db();

  const meetupsCollection = db.collection('meetups');

  const meetups = await meetupsCollection.find().toArray(); 

  client.close();
  
  return{
    props: {
      meetups: meetups.map(meetup => ({
        title:    meetup.title,
        address:  meetup.address,
        image:    meetup.image,
        id: meetup._id.toString(),
      }))
    },
    revalidate: 2
  };
}

export default HomePage