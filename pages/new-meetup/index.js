import { useRouter } from 'next/router';
import { Fragment } from 'react';
import Head from "next/head";


import NewMeetupForm from '../../components/meetups/NewMeetupForm'

function NewMeetupPage(){

  const router = useRouter();

  async function addMeetupHandler(enteredMeetupData){
    console.log(enteredMeetupData);
    const resp = await fetch('/api/new-meetup', {
      method: 'POST',
      body: JSON.stringify(enteredMeetupData),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    const data = await resp.json();
    console.log(data);

    router.push('/');
  }

  return (
    <Fragment>
      <Head>
        <title>Add New Meetup</title>
        <meta 
          name='description'
          content='Add your own meetup'></meta>
      </Head>
      <NewMeetupForm onAddMeetup={addMeetupHandler}></NewMeetupForm>
    </Fragment>
  );
}

export default NewMeetupPage;